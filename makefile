.PHONY: destroy
destroy:
	vagrant destroy
	rm -rf .vagrant
	rm -rf *.log
	rm -rf *.retry

.PHONY: up
up:
	vagrant up

.PHONY: install
install: install-lb
	ansible-playbook -i hosts install.yml
	@echo "Server LB: http://192.174.167.10"
	@echo "Server A: http://192.174.167.101"
	@echo "Server B: http://192.174.167.102"

.PHONY: install-lb
install-lb:
	ansible-playbook -i hosts install-lb.yml
	@echo "Server LB: http://192.174.167.10"
