# Install make

```
apt install make
```

# Install vagrant

## Download vagrant x64

- https://www.vagrantup.com/downloads.html

```
sudo dpkg -i vagrant_2.2.3_x86_64.deb
```

# Install ansible

- https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html#installing-the-control-machine

## Debian

Add the following line to /etc/apt/sources.list:

```
deb http://ppa.launchpad.net/ansible/ansible/ubuntu trusty main
```

Then run these commands:

```
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 93C4A3FD7BB9C367
sudo apt-get update
sudo apt-get install ansible
```

# Destroy vagrant / Clean all 

```
make destroy
```

# Up two servers and one for load balancing

```
make up
```

# Install Apache, Php 7.2, Composer, Symfony 4.2 and load balancing with Nginx


```
make install
```

# AIO

```
make up && make install
```